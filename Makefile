# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: deps watch
deps:
	pip3 install sphinx_zon_theme
	sudo apt install python3-stemmer

RSTS=$(shell ls *.rst)
WATCH_OPTS=$(patsubst %, -f %, $(RSTS))
watch2:
	inotify-hookable $(WATCH_OPTS) -c "make html"

# `pip install sphinx-autobuild` is required
watch:
	sphinx-autobuild . _build/html
