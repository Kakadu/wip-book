Welcome to FP book's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   001intro
   chapter1
   chapter2
   chapter3


А тут мы пишем, что хочется получить

* Программирование в малом
    * Алгебраические типы
        * Нужные какие-то примеры?
    * upgrade с фантомными типами
    * upgrade c GADT
*  То, что нельзя опустить
    * Хвостовая рекурсия
    * CPS
    * ULC
        * Показать late binding на разных стратегиях
    * property-based testing ?
    * deriving ?
* DSL
    * различие shallow и deep
    * пример со стэком и окасаки
    * парсеры и принтеры
* Программирование больших кусков
    * Модули
        * Функторы: показать три вида
    * Final tagless?
    * Расширябельность: объекты, extensible ADT и т.д.
    * Архитектурные паттерны?
        * hash-consing




`A cool website`_

.. _A cool website: http://sphinx-doc.org
.. _ivg1: https://discuss.ocaml.org/t/feedback-on-first-library-before-release/7869/2?u=kakadu
.. _ivg2: https://discuss.ocaml.org/u/ivg/activity
.. _writingEssays: https://rubber-duck-typing.com/articles/2018/09/06/writing-essays/

.. _lightweight_static_g: http://okmij.org/ftp/Computation/lightweight-static-guarantees.html#ADT
.. _alittlebookonAPIdesign: https://people.mpi-inf.mpg.de/~jblanche/api-design.pdf
.. _about_tests: https://mobile.twitter.com/michael_w_hicks/status/1405886245994045441
.. _extensible_types: https://stackoverflow.com/a/54872172/1065436

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
