Часть 3 (Свалка)
================


Что добавить
------------

Functor
-------

Alternative
-----------

Monad
-----


Final-tagless style
-------------------

Ввод-вывод
----------

* Почему hello world стандартный плох. `Видео от создателя Node.js`_
* iteratee (`Iteratees на medium`_)
* conduit



.. _Iteratees на medium: https://medium.com/permutive/towards-a-better-api-for-i-o-35d385060a5c
.. _Видео от создателя Node.js: https://www.youtube.com/watch?v=EeYvFl7li9E

.. note::

    Example: This function is not suitable for sending spam e-mails.
    You can use ``backticks`` for showing ``highlighted`` code.


[Beyond Church encoding: Boehm-Berarducci isomorphism of algebraic data types and polymorphic lambda-terms](https://okmij.org/ftp/tagless-final/course/Boehm-Berarducci.html)

..
  `A cool website`_
