GADTs
=====


// Subsection
// ~~~~~~~~~~

Примеры

* Равенство
* Оптимизированные массивы https://blog.janestreet.com/why-gadts-matter-for-performance/
* STLC-like interpreter https://stackoverflow.com/questions/22676975/
* Глава унижки из Кембриджа https://www.cl.cam.ac.uk/teaching/1415/L28/gadts.pdf
* https://stackoverflow.com/questions/41797085/writing-an-interpreter-with-ocaml-gadts
* https://ocaml.org/meetings/ocaml/2013/proposals/formats-as-gadts.pdf
* https://www.irif.fr/~gradanne/papers/gadts.pdf Drup's slides
